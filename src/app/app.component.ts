import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

// import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { AppService } from './app.service';
import { AuthService } from '../providers/auth-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage = LoginPage;
  restaurant : any;
  auth : any;

  constructor(
    public platform: Platform,
    private appServ: AppService,
    public authServ: AuthService
    ) {}

  ngOnInit() {
    this.platform.ready().then(() => {
      // this.appServ.getLocation();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });

    //get all restaurant
    this.authServ.auth.subscribe(auth => {
      this.auth = auth;
      if(auth){
        this.appServ.getAllRestaurant();
        this.appServ.getAllRestaurants();
        this.appServ.getAllReservations(auth);
        this.appServ.getAdvanceReservations(auth);
        this.appServ.getAllReservationsNotification(auth);        
      };
    });
  }
}
