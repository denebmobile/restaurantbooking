import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {  AngularFireAuth, AngularFireDatabase, AngularFire, FirebaseListObservable } from 'angularfire2';
import * as firebase from 'firebase';
import { AlertController } from 'ionic-angular';
import { Geolocation } from 'ionic-native';
import { Storage } from '@ionic/storage';
declare var google: any;
/*
  Generated class for the AuthData provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AppService {

restaurant : any;
reservations : any;
reservations2 : any;
restaurants : any;
advance_reservations : any;
notification_msg : any;
currentLocation : any;

  constructor(
      public http: Http,
      public db: AngularFireDatabase, 
      public auth: AngularFireAuth,
      public af: AngularFire,
      public alertCtrl: AlertController,
      public storage: Storage
      ) {

    console.log('Hello AppServ Provider');

  }

  getAllRestaurant(){
      return this.restaurant = this.db.list('restaurants');
  }
  getAllRestaurants(){
      this.db.list('restaurants').subscribe((all_restaurants)=>{
        this.restaurants = all_restaurants;
      });
  }

  getAllReservations(auth){
      this.db.list('reservations',{
            query: {
                orderByChild: 'user_id',
                equalTo: auth.uid
            }
        })
        .subscribe((all_reservations)=>{
          this.reservations = all_reservations;
          console.log('abc',all_reservations);
        });
  }

    getAllReservationsNotification(auth){
        let ref = firebase.database().ref('reservations');
        let reservation_query = ref.orderByChild('user_id').on('child_changed',(snapshot)=>{
          console.log('snapshot.key',snapshot.key);
          console.log('snapshot.val()',snapshot.val());
          if(snapshot.val().user_id == auth.uid){
            this.showPrompt(snapshot.val().remarks);
          }
        });
    }

  getAdvanceReservations(auth){
    if(auth){
      console.log('auth uid',auth.uid);
      this.advance_reservations =this.db.list('reservations',{
            query: {
                orderByChild: 'user_id',
                equalTo: auth.uid
            }
        }).map( (arr) => { return arr.reverse(); } );;
    }
  }

  getLocation(){
    let minZoomLevel = 15;
     Geolocation.getCurrentPosition().then((position) => {
         let pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          console.log('position',position);
       let latlng = new google.maps.LatLng(pos.lat, pos.lng);
        let geocoder = new google.maps.Geocoder;
        let storage = this.storage;
        geocoder.geocode({'latLng': latlng}, function(results, status) {
          console.log('rese',results);
          console.log('results',results[0].formatted_address);
          let address = '';
          if(results){            
            for(let i = 0; i < results[0].address_components.length; i++){
              if(results[0].address_components[i].types[0] == 'locality'){
                address += results[0].address_components[i].long_name+", ";
              }
              if(results[0].address_components[i].types[0] == 'administrative_area_level_2'){
                address += results[0].address_components[i].long_name+" ";
              }
              
            }
          }else{
            address = "Talamban, Cebu City"
          }
          sessionStorage.setItem('my_address',address);
          storage.set('my_address',address);          
        });       
     });
  }

  showPrompt(msg) {
      let confirm = this.alertCtrl.create({
        title: 'You Have Notification',
        message: 'msg: '+msg,
        buttons: [
          {
            text: 'Ok',
            handler: () => {                
                
            }
          }
        ]
      });
      confirm.present();
    }

}
