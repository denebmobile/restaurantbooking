import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
// Import AngularFire2
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';
import { Storage } from '@ionic/storage';

// import services
import { PlaceService } from '../services/place-service';
import { CollectionService } from '../services/collection-service';
import { ReviewService } from '../services/review-service';
import { AuthData } from '../providers/auth-data';
import { EventData } from '../providers/event-data';
import { UserService } from '../providers/user-service';
import { AuthService } from '../providers/auth-service';
import { AppService } from '../app/app.service';
import { ReservationService } from '../pages/reservation/reservation.service';
import { FeedService } from '../pages/feed/feed-service';
import { HistoryService } from '../pages/history/history-service';
import { MyHistoryService } from '../pages/my-history/my-history-service';

// end import services

// import pipes
import { StringTimePipe } from '../pipes/string-time.pipe';

// import pages
import {AccountPage} from '../pages/account/account';
import {AddReviewPage} from '../pages/add-review/add-review';
import {BookmarksPage} from '../pages/bookmarks/bookmarks';
import {CollectionsPage} from '../pages/collections/collections';
import {FeedPage} from '../pages/feed/feed';
import {FiltersPage} from '../pages/filters/filters';
import {ForgotPasswordPage} from '../pages/forgot-password/forgot-password';
import {HomePage} from '../pages/home/home';
import {LoginPage} from '../pages/login/login';
import {MainTabsPage} from '../pages/main-tabs/main-tabs';
import {MapPage} from '../pages/map/map';
import {MenuPage} from '../pages/menu/menu';
import {NearbyPage} from '../pages/nearby/nearby';
import {PhotosPage} from '../pages/photos/photos';
import {PlaceDetailPage} from '../pages/place-detail/place-detail';
import {PlacesPage} from '../pages/places/places';
import {ReviewsPage} from '../pages/reviews/reviews';
import {SearchPage} from '../pages/search/search';
import {SelectLocationPage} from '../pages/select-location/select-location';
import {SignUpPage} from '../pages/sign-up/sign-up';
import {UserPage} from '../pages/user/user';
import {ReservationPage} from '../pages/reservation/reservation';
import {HistoryPage} from '../pages/history/history';
import {MyHistoryPage} from '../pages/my-history/my-history';
// end import pages

// AngularFire2 Settings
const firebaseConfig = {
  apiKey: "AIzaSyCLroQCwA5k0LhuMAw7drA9E5TWubfjJwg",
  authDomain: "tablemaps-854af.firebaseapp.com",
  databaseURL: "https://tablemaps-854af.firebaseio.com",
  storageBucket: "tablemaps-854af.appspot.com",
  messagingSenderId: "885323499449"
}

const firebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    StringTimePipe,
    AccountPage,
    AddReviewPage,
    BookmarksPage,
    CollectionsPage,
    FeedPage,
    FiltersPage,
    ForgotPasswordPage,
    HomePage,
    LoginPage,
    MainTabsPage,
    MapPage,
    MenuPage,
    NearbyPage,
    PhotosPage,
    PlaceDetailPage,
    PlacesPage,
    ReviewsPage,
    SearchPage,
    SelectLocationPage,
    SignUpPage,
    UserPage,
    ReservationPage,
    HistoryPage,
    MyHistoryPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {
        platforms: {
          android: {
            tabsPlacement: 'top',
            tabsLayout: 'title-hide'
          },
          windows: {
            tabsLayout: 'title-hide'
          }
        }
      }
    ),
    AngularFireModule.initializeApp(firebaseConfig, firebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AccountPage,
    AddReviewPage,
    BookmarksPage,
    CollectionsPage,
    FeedPage,
    FiltersPage,
    ForgotPasswordPage,
    HomePage,
    LoginPage,
    MainTabsPage,
    MapPage,
    MenuPage,
    NearbyPage,
    PhotosPage,
    PlaceDetailPage,
    PlacesPage,
    ReviewsPage,
    SearchPage,
    SelectLocationPage,
    SignUpPage,
    UserPage,
    ReservationPage,
    HistoryPage,
    MyHistoryPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthData,
    EventData,
    UserService,
    PlaceService,
    CollectionService,
    ReviewService,
    AuthService,
    AppService,
    ReservationService,
    FeedService,
    HistoryService,
    MyHistoryService,
    Storage
  ]
})
export class AppModule {}
