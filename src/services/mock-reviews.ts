export let REVIEWS = [
  {
    id: 1,
    rating: 4.5,
    text: "I actually like it here. I recommend it to all my friends!",
    time: "2 hours ago",
    liked: false,
    commented: true,
    num_likes: 0,
    num_comments: 0,
    photos: [
      {
        thumb: "assets/img/menu/r1_thumb.jpg",
        full_size: "assets/img/menu/r1.jpg"
      }
    ],
    author: {
      id: 1,
      username: "Glenn Paul Contawe",
      profile_picture: "assets/img/user/adam.jpg",
      num_reviews: 50,
      num_folowers: 111
    },
    place: {
      id: 1,
      name: "Patzeria Perfect Pizza",
      district: "Theater District",
      city: "New York City",
      photo: "assets/img/menu/r1_thumb.jpg",
    }
  },
  {
    id: 2,
    rating: 3.5,
    text: "Oh my God! This is a heaven on earth kind of a restaurant. Thumbs up!",
    time: "2 hours ago",
    liked: false,
    commented: true,
    num_likes: 0,
    num_comments: 0,
    photos: [
      {
        thumb: "assets/img/menu/r2_thumb.jpg",
        full_size: "assets/img/menu/r2.jpg"
      }
    ],
    author: {
      id: 1,
      username: "Al Chris Culminas",
      profile_picture: "assets/img/user/ben.png",
      num_reviews: 50,
      num_folowers: 111
    },
    place: {
      id: 1,
      name: "Patzeria Perfect Pizza",
      district: "Theater District",
      city: "New York City",
      photo: "assets/img/menu/r2_thumb.jpg",
    }
  },
  {
    id: 3,
    rating: 4.5,
    text: "Very affordable to students like me.",
    time: "2 hours ago",
    liked: false,
    commented: true,
    num_likes: 0,
    num_comments: 0,
    photos: [
      {
        thumb: "assets/img/menu/r3_thumb.jpg",
        full_size: "assets/img/menu/r3.jpg"
      }
    ],
    author: {
      id: 1,
      username: "Jhun Luis Laurente",
      profile_picture: "assets/img/user/max.png",
      num_reviews: 50,
      num_folowers: 111
    },
    place: {
      id: 1,
      name: "Patzeria Perfect Pizza",
      district: "Theater District",
      city: "New York City",
      photo: "assets/img/menu/r3_thumb.jpg",
    }
  }
]