export let PLACES = [
  {
    id: 1,
    name: "Seafood Island",
    district: "Ayala Mall",
    city: "Cebu City",
    website: "http://seafoodisland.com",
    phone: "+84988888888",
    num_reviews: 30,
    num_photos: 32,
    num_bookmarks: 27,
    num_check_in: 1316,
    num_votes: 232,
    bookmarked: false,
    address: "L4, Outside, 411 , Ayala Mall Cebu",
    rating: 4.1,
    cuisines: "Seafood, Filipino, American",
    cost: 20,
    accepted_card: 1,
    accepted_cash: 1,
    highlights: ['Breakfast', 'Takeaway available'],
    working_hours: {
      monday: {
        from: 7,
        to: 24
      },
      tuesday: {
        from: 7,
        to: 24
      },
      wednesday: {
        from: 7,
        to: 24
      },
      thursday: {
        from: 7,
        to: 24
      },
      friday: {
        from: 7,
        to: 24
      },
      saturday: {
        from: 7,
        to: 24
      },
      sunday: {
        from: 7,
        to: 24
      }
    },
    menu: [
      {
        id: 1,
        name: "Soup",
        items: [
          {
            id: 1,
            name: "Grilled Shrimp Scampi",
            price: 200
          },
          {
            id: 2,
            name: "Fish Tacos",
            price: 350
          },
          {
            id: 3,
            name: "Tilapia Scampi",
            price: 400
          }
        ]
      },
      {
        id: 2,
        name: "Salad",
        items: [
          {
            id: 4,
            name: "Small Ceasar Salad",
            price: 6.95
          },
          {
            id: 5,
            name: "Ceasar Salad with Grilled Steak",
            price: 13.95
          },
          {
            id: 6,
            name: "Ceasar Salad"
          }
        ]
      },
      {
        id: 3,
        name: "Pizza",
        items: [
          {
            id: 7,
            name: "Neopolitan Pizza",
            price: 3
          },
          {
            id: 8,
            name: "Sicilian Slice Pie",
            price: 4
          },
          {
            id: 9,
            name: "Sicilian Pizza",
            price: 28
          }
        ]
      }
    ],
    reviews: [
      {
        id: 1,
        username: "Jhun Laurente",
        profile_picture: "assets/img/user/adam.jpg",
        rating: 4.5,
        comment: "Go ahead – take it for a spin, and yes, never have a bad meal!"
      },
      {
        id: 2,
        username: "Glenn Contawe",
        profile_picture: "assets/img/user/ben.png",
        rating: 4.5,
        comment: "I love it here!"
      }
    ],
    photos: [
      {
        thumb: "assets/img/menu/r1_thumb.jpg",
        full_size: "assets/img/menu/r1.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r2_thumb.jpg",
        full_size: "assets/img/menu/r2.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r3_thumb.jpg",
        full_size: "assets/img/menu/r3.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r4_thumb.jpg",
        full_size: "assets/img/menu/r4.jpg",
        description: ""
      }
    ]
  },
  {
    id: 2,
    name: "Wasted Chef",
    district: "Gorordo Ave.",
    city: "Cebu City",
    website: "wastedchef.com",
    phone: "+09951222107",
    num_reviews: 193,
    num_photos: 32,
    num_bookmarks: 27,
    num_check_in: 1316,
    num_votes: 232,
    bookmarked: false,
    address: "Taft Business Center, 6000, 96 F Gorordo Ave, Cebu City",
    rating: 4.1,
    cuisines: "Pizza, Pasta, Burger, Shawarma",
    cost: 20,
    accepted_card: 1,
    accepted_cash: 1,
    highlights: ['Breakfast Lunch Dinner', 'Takeaway available', 'Buffet'],
    working_hours: {
      monday: {
        from: 7,
        to: 24
      },
      tuesday: {
        from: 7,
        to: 24
      },
      wednesday: {
        from: 7,
        to: 24
      },
      thursday: {
        from: 7,
        to: 24
      },
      friday: {
        from: 7,
        to: 24
      },
      saturday: {
        from: 7,
        to: 24
      },
      sunday: {
        from: 7,
        to: 24
      }
    },
    menu: [
      {
        id: 1,
        name: "Soup",
        items: [
          {
            id: 1,
            name: "Pizza",
            price: 4.95
          },
          {
            id: 2,
            name: "Pasta",
            price: 5.95
          },
          {
            id: 3,
            name: "Shrimp",
            price: 4.95
          }
        ]
      },
      {
        id: 2,
        name: "Salad",
        items: [
          {
            id: 4,
            name: "Small Ceasar Salad",
            price: 6.95
          },
          {
            id: 5,
            name: "Ceasar Salad with Grilled Steak",
            price: 13.95
          },
          {
            id: 6,
            name: "Ceasar Salad"
          }
        ]
      },
      {
        id: 3,
        name: "Pizza",
        items: [
          {
            id: 7,
            name: "Neopolitan Pizza",
            price: 3
          },
          {
            id: 8,
            name: "Sicilian Slice Pie",
            price: 4
          },
          {
            id: 9,
            name: "Sicilian Pizza",
            price: 28
          }
        ]
      }
    ],
    reviews: [
      {
        id: 1,
        username: "Ovuvwevwe",
        profile_picture: "assets/img/user/adam.jpg",
        rating: 4.5,
        comment: "I have never been there"
      },
      {
        id: 2,
        username: "Christian Do",
        profile_picture: "assets/img/user/ben.png",
        rating: 4.5,
        comment: " yes, neer have a bad meal!"
      }
    ],
    photos: [
      {
        thumb: "assets/img/menu/r4_thumb.jpg",
        full_size: "assets/img/menu/r4.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r5_thumb.jpg",
        full_size: "assets/img/menu/r5.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r6_thumb.jpg",
        full_size: "assets/img/menu/r6.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r7_thumb.jpg",
        full_size: "assets/img/menu/r7.jpg",
        description: ""
      }
    ]
  },
  {
    id: 3,
    name: "Pabugnawan",
    district: "Talamban",
    city: "Cebu City",
    website: "http://pabugnawan.com",
    phone: "+639165870406",
    num_reviews: 193,
    num_photos: 32,
    num_bookmarks: 27,
    num_check_in: 1316,
    num_votes: 232,
    bookmarked: false,
    address: "USC, Talamban Campus, Cebu City",
    rating: 4.5,
    cuisines: "Refreshments",
    cost: 20,
    accepted_card: 1,
    accepted_cash: 1,
    highlights: ['Drinks', 'Takeaway available'],
    working_hours: {
      monday: {
        from: 7,
        to: 24
      },
      tuesday: {
        from: 7,
        to: 24
      },
      wednesday: {
        from: 7,
        to: 24
      },
      thursday: {
        from: 7,
        to: 24
      },
      friday: {
        from: 7,
        to: 24
      },
      saturday: {
        from: 7,
        to: 24
      },
      sunday: {
        from: 7,
        to: 24
      }
    },
    menu: [
      {
        id: 1,
        name: "Mango Juice",
        items: [
          {
            id: 1,
            name: "Apple Juice",
            price: 20
          },
          {
            id: 2,
            name: "Lime Juice",
            price: 20
          },
          {
            id: 3,
            name: "Juicy Fruit",
            price: 20
          }
        ]
      },
      {
        id: 2,
        name: "Salad",
        items: [
          {
            id: 4,
            name: "Small Ceasar Salad",
            price: 6.95
          },
          {
            id: 5,
            name: "Ceasar Salad with Grilled Steak",
            price: 13.95
          },
          {
            id: 6,
            name: "Ceasar Salad"
          }
        ]
      },
      {
        id: 3,
        name: "Pizza",
        items: [
          {
            id: 7,
            name: "Neopolitan Pizza",
            price: 3
          },
          {
            id: 8,
            name: "Sicilian Slice Pie",
            price: 4
          },
          {
            id: 9,
            name: "Sicilian Pizza",
            price: 28
          }
        ]
      }
    ],
    reviews: [
      {
        id: 1,
        username: "Lean Goopio",
        profile_picture: "assets/img/user/adam.jpg",
        rating: 4.5,
        comment: "Good Lord!"
      },
      {
        id: 2,
        username: "Christian Anayron",
        profile_picture: "assets/img/user/ben.png",
        rating: 4.5,
        comment: "I love it!"
      }
    ],
    photos: [
      {
        thumb: "assets/img/menu/r6_thumb.jpg",
        full_size: "assets/img/menu/r6.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r2_thumb.jpg",
        full_size: "assets/img/menu/r2.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r1_thumb.jpg",
        full_size: "assets/img/menu/r1.jpg",
        description: ""
      },
      {
        thumb: "assets/img/menu/r4_thumb.jpg",
        full_size: "assets/img/menu/r4.jpg",
        description: ""
      }
    ]
  }
];