import {Injectable} from "@angular/core";
import {PLACES} from "./mock-places";
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {  AngularFireAuth, AngularFireDatabase, AngularFire } from 'angularfire2';

@Injectable()
export class PlaceService {
  private places: any;

  constructor(public http: Http,public db: AngularFireDatabase, public auth: AngularFireAuth,public af: AngularFire) {
    this.places = PLACES;
  }

  getAll() {
    return this.places;
  }

  getItem(id) {
    for (var i = 0; i < this.places.length; i++) {
      if (this.places[i].id === parseInt(id)) {
        return this.places[i];
      }
    }
    return null;
  }

  remove(item) {
    this.places.splice(this.places.indexOf(item), 1);
  }
}