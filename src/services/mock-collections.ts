/**
 * Created by succe on 09-Jul-16.
 */
export let COLLECTIONS = [
  {
    id: 1,
    name: "Trending this Week",
    num_places: 30,
    num_bookmarks: 6268,
    background: "assets/img/menu/r1.jpg",
    places: [1, 2, 3, 4]
  },
  {
    id: 2,
    name: "Barbecue",
    num_places: 30,
    num_bookmarks: 6268,
    background: "assets/img/menu/r2.jpg",
    places: [1, 2, 3, 4]
  },
  {
    id: 3,
    name: "Cafe",
    num_places: 30,
    num_bookmarks: 6268,
    background: "assets/img/menu/r3.jpg",
    places: [1, 2, 3, 4]
  },
  {
    id: 4,
    name: "Buffet",
    num_places: 30,
    num_bookmarks: 6268,
    background: "assets/img/menu/r1.jpg",
    places: [1, 2, 3, 4]
  }
]