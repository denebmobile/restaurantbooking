import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'stringTime'
})
export class StringTimePipe implements PipeTransform {

  transform(time: any): any {
    let index = time.indexOf(':');

    let hours = time.substring(0, index);
    let minutes = time.substring(index + 1);
    let ampm;

    if (+hours >= 12) {
      if (+hours == 24) {
        hours = 12;
        ampm = 'am';
      } else {
        if (+hours > 12) {
          hours = +hours - 12;
        }
        ampm = 'pm';
      }
    } else if (+hours < 1) {
      hours = 12;
      ampm = 'am';
    } else {
      ampm = 'am';
    }

    let final_time = hours + ':' + minutes + ' ' + ampm;
    
    return final_time;
  }

}
