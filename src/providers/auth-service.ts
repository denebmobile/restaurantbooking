import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {  AngularFireAuth, AngularFireDatabase, AngularFire } from 'angularfire2';
import * as firebase from 'firebase';

/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {
	constructor(public http: Http,public db: AngularFireDatabase, public auth: AngularFireAuth,public af: AngularFire) {
		console.log('Hello AuthService Provider');
	}
	login(loginForm){
		return this.auth.login(loginForm);
	}
	logout() {
		return this.af.auth.logout();
	}
	register(registerForm){
		return this.auth.createUser(registerForm);
	}
	registerProfile(uid,registerForm){
		return this.db.list('/users').update(uid,registerForm);
	}
	setNewPassword(email){
		return firebase.auth().sendPasswordResetEmail(email);
	}
}
