import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {  AngularFireAuth, AngularFireDatabase, AngularFire } from 'angularfire2';

/*
  Generated class for the UserService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserService {
	constructor(public http: Http,public db: AngularFireDatabase, public auth: AngularFireAuth,public af: AngularFire) {
		console.log('Hello UserService Provider');
	}
	getUserProfile(user_id){
		return this.db.object('users/' + user_id);
	}

}
