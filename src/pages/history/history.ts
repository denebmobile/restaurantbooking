import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ReviewService } from '../../services/review-service';
import { PlaceDetailPage } from '../place-detail/place-detail';
import { AppService } from '../../app/app.service';
import { HistoryService } from './history-service';

import * as lodash from 'lodash';

/*
  Generated class for the History page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-history',
  templateUrl: 'history.html'
})
export class HistoryPage {
  public reviews: any;
  all_reservations : any;
  loader : any;
  restaurants : any;
  date_today : any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public reviewService: ReviewService,
    private appServ: AppService,
    public loadingController : LoadingController,
    public historyServ : HistoryService,
    public alertCtrl: AlertController,
  ) {

    this.loader = this.loadingController.create({
      content: ""
    });
    this.loader.present();
    let rightNow = new Date();
    this.date_today = rightNow.toISOString().slice(0,10);
    this.restaurants = this.appServ.restaurants;
    this.appServ.advance_reservations.subscribe((get_reservations)=>{
      for(let i = 0; i < get_reservations.length; i++){
        let tempStoragePast = lodash.find(this.restaurants, {'$key': get_reservations[i].restaurant_id});
        if(tempStoragePast){
          get_reservations[i]['restaurant_name'] = tempStoragePast['name'];
          get_reservations[i]['restaurant_address'] = tempStoragePast['address'];
        }
      }
      this.all_reservations = get_reservations;
    });   
  }

  ionViewDidLoad() {
    this.loader.dismiss();
    console.log('ionViewDidLoad HistoryPage');
  }

  viewPlace(id) {
    this.navCtrl.push(PlaceDetailPage, {id: id});
  }

  cancelReservation(reservation_id){
    this.showPrompt(reservation_id);
  }

  showPrompt(reservation_id) {
      let confirm = this.alertCtrl.create({
        title: 'Warning!',
        message: "Are you sure you want to delete this reservation?",
        buttons: [
          {
            text: 'Yes',
            handler: () => {                
                this.historyServ.cancelReservation(reservation_id).then((data) => {
                  
                });
            }
          },
          {
            text: 'No',
            handler: () => {
             
            }
          }
        ]
      });
      confirm.present();
    }

}
