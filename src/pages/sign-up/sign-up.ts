import { Component } from '@angular/core';
import { NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';
import { FormBuilder, Validators,FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { Storage } from '@ionic/storage';
// Service Providers
import { AuthService } from '../../providers/auth-service';

// Validator
import { emailValidator } from '../../validators/validators';

import {ForgotPasswordPage} from '../forgot-password/forgot-password';
import {MainTabsPage} from '../main-tabs/main-tabs';
/*
  Generated class for the SignUp page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html'
})
export class SignUpPage {
  public registerForm:FormGroup = this.fb.group({
    firstname:['',Validators.required],
    lastname:['',Validators.required],
    email: ['', Validators.compose([Validators.required,  emailValidator])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  });
  public account: any;
  public loader: any;

  constructor(public navCtrl: NavController, 
		public navParams: NavParams, 
		public fb: FormBuilder, 
		public authService: AuthService, 
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		public storage: Storage) {}
  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }
  register(registerFormData){
    this.loader = this.loadingCtrl.create({
      content: "Registering..."
    });
    console.log("Register Form DAta:",registerFormData.value);
    if(registerFormData.valid){
      this.loader.present();
      registerFormData.value.email = _.toLower(registerFormData.value.email);
      this.account = {
        email:registerFormData.value.email,
        password:registerFormData.value.password
      };
      this.authService.register(this.account)
        .then((user)=>{
          console.log("user",user);
          console.log("userid:",user.uid);
          delete registerFormData.value.password;
          registerFormData.value.role = 'user';
          this.registerProfile(user.uid,registerFormData.value).then((success)=>{
            console.log("Success:",success)
            this.loader.dismiss();
            this.onRegisterSuccess(success);
          }).catch((error)=>{
            console.log("Error Profile:",error.message);
            this.alertPopUp("Error Profile creation.",error.message);
          });
        }).catch((error)=>{
          console.log("Error Registration",error.message);
          this.alertPopUp("Error Registration",error.message);
          this.loader.dismiss();
        });
      }else{
        this.alertPopUp("Form is invalid!","Please fill up the necessary fields.");
      }
	    
  	}
  registerProfile(uid,profile){
    return this.authService.registerProfile(uid,profile);
  }
 	private onRegisterSuccess(success): void {    
 		this.loader = this.loadingCtrl.create({
      content: "Signing in..."
    });
 		let prompt = this.alertPopUp("Registration Successful!","You have successfully created an account!");
 		this.registerForm.reset();
 		setTimeout(()=>{
			prompt.dismiss();
	 		this.loader.present();
	 		this.authService.login(this.account).then((success)=>{
	 			console.log("Login Sucess:",success);
				this.storage.set('email', this.account.email);
				this.storage.set('uid', success.uid);

	 			this.loader.dismiss();
	 			this.navCtrl.push(MainTabsPage);
	 		}).catch((err)=>{
	 			console.log("Error Login:",err.message);
	 		});
 		},2000);
 		
 	}
  private alertPopUp(title,message){
		let prompt = this.alertCtrl.create({
			title: title,
			message: message,
			buttons: ['Ok']
		});
		prompt.present();
		return prompt;
	}
  // go to forgot password page
  forgotPwd() {
    this.navCtrl.push(ForgotPasswordPage);
  }


}
