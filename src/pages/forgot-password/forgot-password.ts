import { Component } from '@angular/core';
import { NavController, NavParams,LoadingController,AlertController } from 'ionic-angular';
import { FormBuilder, Validators,FormGroup } from '@angular/forms';
import {AuthData} from '../../providers/auth-data';
import * as _ from 'lodash';
// Service Providers
import { AuthService } from '../../providers/auth-service';
/*
  Generated class for the ForgotPassword page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
  public resetPasswordForm:FormGroup;
  emailChanged: boolean = false;
  passwordChanged: boolean = false;
  submitAttempt: boolean = false;
  
  constructor(public authData: AuthData, public formBuilder: FormBuilder,
    public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,public auhService:AuthService) {
  	  
  	  this.resetPasswordForm = formBuilder.group({
  		  email: ['', Validators.compose([Validators.required])],
  	  })
    }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }
  reset(resetPasswordData){
    if(resetPasswordData.valid){
      console.log("Email:",resetPasswordData.value);
      resetPasswordData.value.email = _.toLower(resetPasswordData.value.email);
      this.auhService.setNewPassword(resetPasswordData.value.email).then((success)=>{
        console.log("Success:",success);
        this.alertPopUp("Password reset sent to email!","Please check your email to change your password.");
      }).catch((error)=>{
        console.log("Error:",error.message);
        this.alertPopUp("Error",error.message);
      });
    }else{
      this.alertPopUp("Form is invalid!","Please fill up the necessary fields.");
    }
  }
  private alertPopUp(title,message){
		let prompt = this.alertCtrl.create({
			title: title,
			message: message,
			buttons: ['Ok']
		});
		prompt.present();
		return prompt;
	}
}
