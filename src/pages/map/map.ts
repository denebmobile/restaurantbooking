import { Component } from '@angular/core';
import { NavController, NavParams,Platform } from 'ionic-angular';
declare var google: any;
/*
  Generated class for the Map page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
  public map: any;
  address : any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public platform: Platform) {
      // init map
      this.address = window.sessionStorage.getItem('place_address');
      
  }

  ionViewDidLoad() {
    this.initializeMap();
    console.log('ionViewDidLoad MapPage');
  }

  initializeMap() {
    let minZoomLevel = 15;
    
    let geocoder = new google.maps.Geocoder(); 
    if(geocoder){
        geocoder.geocode({
          'address': this.address
        }, function(results, status) {
          console.log('results',results);
            this.map = new google.maps.Map(document.getElementById('map_canvas'), {
              zoom: minZoomLevel,
              center: results[0].geometry.location,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }); 

            let marker = new google.maps.Marker({
              position: results[0].geometry.location,
              map: this.map
            });
        });
    }
  }
}
