import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { CollectionsPage } from '../collections/collections';
import { FeedPage }from '../feed/feed';
import { HistoryPage }from '../history/history';
import { AccountPage } from '../account/account';
/*
  Generated class for the MainTabs page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-main-tabs',
  templateUrl: 'main-tabs.html'
})
export class MainTabsPage {
  public tabHome = HomePage;
  public tabCollections = CollectionsPage;
  public tabHistory = HistoryPage;
  public tabAccount = AccountPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainTabsPage');
  }

}
