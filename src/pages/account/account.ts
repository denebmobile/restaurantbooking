import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { App } from 'ionic-angular';
import { Subscription } from 'rxjs';
// Service Providers
import { AuthService } from '../../providers/auth-service';
import { UserService } from '../../providers/user-service';

// Pages
import { LoginPage } from '../login/login';
import { MyHistoryPage } from '../my-history/my-history';
/*
  Generated class for the Account page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
  public user_id:String;
  public user_data:any;
  public user_data_subscription:Subscription;
  public loader:any;
  constructor(public navCtrl: NavController, 
  	public navParams: NavParams,
  	public storage: Storage,
  	public authService: AuthService,
  	public loadingCtrl: LoadingController,
  	public appCtrl: App,
    public userService:UserService) {
      
    }
  ionViewWillEnter(){
    this.loader = this.loadingCtrl.create({
			content: ''
		});
    this.loader.present();
    this.storage.get('uid').then((uid)=>{
      this.user_id = uid;
      this.user_data_subscription = this.userService.getUserProfile(this.user_id).subscribe((user)=>{
        this.user_data = user;
        console.log("User Data:",user);
        this.loader.dismiss();
      });
    })
  }
  ionViewWillLeave(){
    this.user_data_subscription.unsubscribe();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
  }
  logout(){
    let prompt = this.loadingCtrl.create({
      content: "Signing out.."
    });
    prompt.present();
    this.user_data_subscription.unsubscribe();
  	this.storage.remove('email');
  	this.storage.remove('uid').then((uid) => {
    this.authService.logout();
    setTimeout(()=>{
        prompt.dismiss();
        this.appCtrl.getRootNav().setRoot(LoginPage);
      },2000);  
    });
  }

  goToHist(){
    this.navCtrl.push(MyHistoryPage);
  }

}
