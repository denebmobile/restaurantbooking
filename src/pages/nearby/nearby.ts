import { Component } from '@angular/core';
import { NavController, NavParams,Platform, LoadingController } from 'ionic-angular';
import { AppService } from '../../app/app.service';
import { Subscription } from 'rxjs';
declare var google: any;
import { Geolocation } from 'ionic-native';
/*
  Generated class for the Nearby page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-nearby',
  templateUrl: 'nearby.html'
})
export class NearbyPage {
  public map: any;
  loader : any;
  vendors_subscription: Subscription;
  restaurants : any;

  constructor(
    public nav: NavController, 
    public platform: Platform, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private appServ: AppService
    ) {
      this.restaurants = this.appServ.restaurants;
    }

   ionViewDidLoad() {
    console.log('ionViewDidLoad NearbyPage');
   }

   ionViewWillEnter() {
      this.loader = this.loadingCtrl.create({content:""});
      this.loader.present();
      this.loadMap();
    }

    ionViewDidLeave(){
      this.map = null || "";
    }

    loadMap(){
      Geolocation.getCurrentPosition().then((position) => {
        //original
        let pos = {
          lat: position.coords.latitude,
          lng: position.coords.longitude
        };
        //test
        // let pos = {
        //   lat: 10.3541,
        //   lng: 123.9116
        // };
        let minZoomLevel = 12;        
        console.log('pos',pos);
        let arr = [];
        for(let i = 0; i < this.restaurants.length; i++){
          arr[i] = this.restaurants[i].address;
        }        
        let service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
        {
          origins: [pos],
          destinations: arr,
          travelMode: 'DRIVING',
        },(response, status) =>{
          console.log('distance matrix response',response);
          for(let i = 0; i < this.restaurants.length; i++){
            this.restaurants[i]['distance'] = response['rows'][0]['elements'][i]['distance']['text'];
          }
          let geocoder = new google.maps.Geocoder(); 
          let with_in = [];
          for(let i = 0; i < this.restaurants.length; i++){
            let infowindow = new google.maps.InfoWindow();
            let distance = this.restaurants[i].distance.split(" ");
            if(distance[0] <= 10){
              if(geocoder){
                console.log('fak',this.restaurants[i].name);
                let restaurant_name = this.restaurants[i].name;
                geocoder.geocode({
                  'address': this.restaurants[i].address
                }, function(results, status) {
                  console.log('latlng',results[0].geometry.location);
                  if(!this.map){
                    this.map = new google.maps.Map(document.getElementById('map_canvas'), {
                      zoom: minZoomLevel,
                      center: new google.maps.LatLng(pos.lat, pos.lng),
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                    }); 
                  }                  
                  let marker = new google.maps.Marker({
                    position: results[0].geometry.location,
                    map: this.map
                  });
                  console.log('abcd',this.restaurants);
                  google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                      infowindow.setContent(restaurant_name);
                      infowindow.open(this.map, marker);
                    }
                  })(marker, i));
                });
              }
              with_in.push(this.restaurants[i]);
            }
          }  
          console.log('restaurants with in',with_in);       
        });
        if(!this.map){
            this.map = new google.maps.Map(document.getElementById('map_canvas'), {
              zoom: minZoomLevel,
              center: new google.maps.LatLng(pos.lat, pos.lng),
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }); 
          }  
      this.loader.dismiss();
      });
    	
    }

}
