import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController,App } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { Storage } from '@ionic/storage';
import { Subscription } from 'rxjs';
// Validator
import { emailValidator } from '../../validators/validators';
// Service Providers
import { AuthService } from '../../providers/auth-service';

import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { SignUpPage } from '../sign-up/sign-up';
import { MainTabsPage } from '../main-tabs/main-tabs';
/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public loginForm:FormGroup = this.fb.group({
    email: ['', Validators.compose([Validators.required,  emailValidator])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  });
  public loader:any;
  public auth:any;
  public auth_subscription:Subscription;
  constructor(public navCtrl: NavController, 
  public navParams: NavParams,
  public fb: FormBuilder,
  public loadingCtrl:LoadingController,
  public authService:AuthService,
  public alertCtrl: AlertController,
  public storage:Storage,
  public appCtrl:App) {
    
  }
  ionViewWillEnter(){
    this.auth_subscription = this.authService.auth.subscribe((auth)=>{
      if(auth){
        this.auth = auth;
        this.navCtrl.push(MainTabsPage);
      }
    })
  }
  ionViewWillLeave(){
    this.auth_subscription.unsubscribe();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  // go to forgot password page
  forgotPwd() {
    this.navCtrl.push(ForgotPasswordPage);
  }

  // process login
  login(loginFormData) {
		console.log("Login Form Value:",loginFormData.value);
		if(loginFormData.valid){
			this.loginForm.value.email = loginFormData.value.email = _.toLower(loginFormData.value.email);
			this.authService.login(loginFormData.value)
        .then((success) => this.onSignInSuccess(success))
        .catch((error) => this.alertPopUp('Login Failed.',error.message));
		}else{
      this.alertPopUp("Form is invalid!","Please fill up the necessary fields.");
    }
	}
  // go to sign up page
  signUp() {
    // add our sign up code here
    this.navCtrl.push(SignUpPage);
  }

	private onSignInSuccess(success): void {    
		console.log("success logged in",success);
		this.storage.set('email', _.toLower(this.loginForm.value.email));
		this.storage.set('uid', success.uid);
    window.sessionStorage.setItem('user_id',success.uid);
	}
  private alertPopUp(title,message){
		let prompt = this.alertCtrl.create({
			title: title,
			message: message,
			buttons: ['Ok']
		});
		prompt.present();
		return prompt;
	}
}
