import { Component } from '@angular/core';
import { App,NavController, LoadingController } from 'ionic-angular';
import {PlaceService} from '../../services/place-service';
import {SelectLocationPage} from '../select-location/select-location';
import {PlacesPage} from '../places/places';
import {PlaceDetailPage} from '../place-detail/place-detail';
import {SearchPage} from '../search/search';
import {BookmarksPage} from '../bookmarks/bookmarks';
// import {MapPage} from '../map/map';
import {NearbyPage} from '../nearby/nearby';
import { AppService } from '../../app/app.service';
import { Storage } from '@ionic/storage';
declare var google: any;

/*
 Generated class for the LoginPage page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // list slides for slider
  public slides = [
    {
      src: 'assets/img/bugger.jpg'
    }
  ];

  logo : any = 'assets/img/bugger.jpg';

  // list popular places
  public popularPlaces: any;
  place : any;
  restaurant : any;
  loader : any;
  myLocation : any;

  constructor(
    public navCtrl:NavController, 
    public placeService:PlaceService, 
    public app:App,
    private appServ: AppService,
    public loadingController : LoadingController,
    public storage : Storage
    ) {
    this.loader = this.loadingController.create({
      content: ""
    });
    this.popularPlaces = placeService.getAll();
    this.loader.present();
    this.myLocation = window.sessionStorage.getItem('my_address');
    if(!window.sessionStorage.getItem('my_address')){
      this.storage.get('my_address').then((val) => {        
        if(val){
          this.myLocation = val;
        }else{
          this.myLocation = "My Location";
        }
     });
    }    
  }

  ionViewDidLoad() {    
    this.appServ.restaurant.subscribe((all_restaurant) => {
      this.restaurant = all_restaurant;
      console.log(this.restaurant);   
      this.loader.dismiss();    
    });
    

  }

  // go to select location page
  selectLocation() {
    this.navCtrl.push(SelectLocationPage);
  }

  // go to places
  viewPlaces() {
    this.navCtrl.push(PlacesPage);
  }

  // view a place
  viewPlace(restaurant_info) {
    window.sessionStorage.setItem('restaurant_info',JSON.stringify(restaurant_info));
    window.sessionStorage.setItem('restaurant_id',restaurant_info.$key);
    this.navCtrl.push(PlaceDetailPage);   
  }

  // go to search page
  goToSearch() {
    this.navCtrl.push(SearchPage);
  }

  // go to bookmarks page
  goToBookmarks() {
    this.navCtrl.push(BookmarksPage);
  }

  // // view map
  // goToMap() {
  //   // this.app.getRootNav().push(MapPage);
  //   this.navCtrl.push(MapPage);
  // }

  // view nearby page
  goToNearBy() {
    this.navCtrl.push(NearbyPage);
  } 

}
