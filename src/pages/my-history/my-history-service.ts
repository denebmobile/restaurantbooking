import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {  AngularFireAuth, AngularFireDatabase, AngularFire } from 'angularfire2';
/*
  Generated class for the AuthData provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MyHistoryService {


    constructor(
      public http: Http,
      public db: AngularFireDatabase, 
      public auth: AngularFireAuth,
      public af: AngularFire) {

        console.log('Hello FeedService Provider');
    
      }
      cancelReservation(reservation_id){
          console.log(reservation_id);
        let obj = {};
        obj['status'] = 0;
        return this.db.object('reservations/'+reservation_id).update(obj);
      }
}