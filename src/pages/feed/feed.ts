import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {ReviewService} from '../../services/review-service';
import {PlaceDetailPage} from '../place-detail/place-detail';
import { AppService } from '../../app/app.service';
import { FeedService } from './feed-service';

import * as lodash from 'lodash';
/*
  Generated class for the Feed page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html'
})
export class FeedPage {
  public reviews: any;
  all_reservations : any;
  past_reservation : any;
  current_reservation : any;
  restaurant_list : any;
  loader : any;
  restaurants : any;
 
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public reviewService: ReviewService,
    private appServ: AppService,
    public loadingController : LoadingController,
    public feedServ : FeedService
  ) {
    // feed
    this.loader = this.loadingController.create({
      content: ""
    });
    let rightNow = new Date();
    let res = rightNow.toISOString().slice(0,10);
    let hour_now = rightNow.getHours();
    let min_now = rightNow.getMinutes();
    let tempStorage = res.split('-');
    let year_now = tempStorage[0];
    let month_now = tempStorage[1];
    let date_now = tempStorage[2];
    this.past_reservation = [];
    this.current_reservation = [];
    this.reviews = reviewService.getAll();
    this.restaurants = this.appServ.restaurants;
    this.appServ.reservations2.subscribe((all_reservations) => {
      this.all_reservations = all_reservations;
      console.log('this.all_reservations',this.all_reservations);
      console.log('this.restaurant_list',this.restaurants);      
      for(let i = 0; i < this.all_reservations.length; i++){
        let tempStorage = lodash.find(this.restaurants, {'$key': this.all_reservations[i].restaurant_id});
        this.all_reservations[i]['restaurant_name'] = tempStorage['name'];
        this.all_reservations[i]['restaurant_address'] = tempStorage['address'];
        let temp_date = this.all_reservations[i].date.split('-');
        let temp_time = this.all_reservations[i].time.split('-');
        if(parseInt(year_now) > parseInt(temp_date[0]) || this.all_reservations[i].status == 0){
          this.past_reservation.push(this.all_reservations[i]);
          continue;
        }else if(parseInt(year_now) == parseInt(temp_date[0])){
          if(parseInt(month_now) > parseInt(temp_date[1]) || this.all_reservations[i].status == 0){
            this.past_reservation.push(this.all_reservations[i]);
            continue;
          }else if(parseInt(month_now) == parseInt(temp_date[1])){
            if(parseInt(date_now) > parseInt(temp_date[2])  || this.all_reservations[i].status == 0){
              this.past_reservation.push(this.all_reservations[i]);
              continue;
            }else if(parseInt(date_now) == parseInt(temp_date[2])){
              if(hour_now >= parseInt(temp_time[0])  || this.all_reservations[i].status == 0){
                this.past_reservation.push(this.all_reservations[i]);
                continue;
              }else{
                console.log('hour_now = true');
              }
            }
          }
        }
        this.current_reservation.push(this.all_reservations[i]);     
      }
      console.log('current_reservation',this.current_reservation);
      console.log('past_reservation',this.past_reservation);
    });
    
  }

  ionViewDidLoad() {
    // this.loader.present();
    // this.appServ.restaurant.subscribe((all_restaurant) => {
    //   this.restaurant_list = all_restaurant;
    //   console.log('this.restaurant_list',this.restaurant_list);
    //   if(this.past_reservation.length > 0){
        
    //     for(let i = 0; i < this.past_reservation.length; i++){
    //       let tempStoragePast = lodash.find(this.restaurant_list, {'$key': this.past_reservation[i].restaurant_id});
    //       if(tempStoragePast){
    //         this.past_reservation[i]['restaurant_name'] = tempStoragePast['name'];
    //         this.past_reservation[i]['restaurant_address'] = tempStoragePast['address'];
    //       }
    //     }
        
    //   }
    //   if(this.current_reservation.length > 0){
    //     for(let j = 0; j < this.current_reservation.length; j++){
    //       let tempStorageCurrent = lodash.find(this.restaurant_list, {'$key': this.current_reservation[j].restaurant_id});
    //       if(tempStorageCurrent){
    //         this.past_reservation[j]['restaurant_name'] = tempStorageCurrent['name'];
    //         this.past_reservation[j]['restaurant_address'] = tempStorageCurrent['address'];
    //       }
    //     }
    //   }
    //   this.loader.dismiss();
    // });
  }
  // view a place
  viewPlace(id) {
    this.navCtrl.push(PlaceDetailPage, {id: id});
  }

  cancelReservation(reservation_id){
    this.feedServ.cancelReservation(reservation_id)
      .then((data)=>{
        console.log(data);
      })
  }

}
