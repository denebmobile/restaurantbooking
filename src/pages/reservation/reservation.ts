import { Component } from '@angular/core';
import { NavController, NavParams, AlertController,LoadingController } from 'ionic-angular';

import { ReservationService } from './reservation.service';
import { AppService } from '../../app/app.service';
import { Storage } from '@ionic/storage';
import { LocalNotifications } from 'ionic-native';

import * as lodash from 'lodash';

/*
  Generated class for the Reservation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-reservation',
  templateUrl: 'reservation.html'
})
export class ReservationPage {
  date_now : any;
  max_date : any;
  month_now : any;
  year_now :any;
  reservation_date : any;
  reservation_time : any;
  number_people : any;
  available_time : any;
  to_available_time : any;  
  time_end : any;
  available_date : any;
  restaurant_info : any;
  user_id : any;
  loader : any;
  all_reservations : any;
  num_people : any;
  date_storange : any;
  available_people : any;
  restaurant_id : any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public reservationServ : ReservationService,
    public alertCtrl: AlertController,
    public loadingController : LoadingController,
    private appServ: AppService,
    public storage: Storage
    ) {   
   
  }
  ionViewWillEnter(){
     this.loader = this.loadingController.create({
        content: ""
      });
      this.storage.get('uid').then((val) => {
         this.user_id = val;
         console.log('uid',this.user_id);
       });
      
      this.restaurant_info = JSON.parse(window.sessionStorage.getItem('restaurant_info'));
      this.restaurant_id = window.sessionStorage.getItem('restaurant_id');
      this.restaurant_info.$key
      console.log('hmmm',this.restaurant_info.$key);
      // this.loader.present();
      this.all_reservations = this.appServ.reservations;
      console.log('this.all_reservations',this.all_reservations);
      
      // let tempStorage = lodash.find(this.all_reservations, {'restaurant_id': this.restaurant_info.$key});
      //   console.log('find',tempStorage);
      if(this.all_reservations){
        for(let i = 0; i < this.all_reservations.length; i++){
          if(this.all_reservations[i].restaurant_id == this.restaurant_info.$key){
            // if(){
  
            // }
          }
        }
      }
      this.reservationServ.getDates(this.all_reservations).then((dates:any) => {
        this.available_date = dates;
        console.log("av_date",this.available_date);
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReservationPage');
  }

  onChangeDate($event){
    this.date_storange = $event;
    this.reservationServ.getTime($event,this.all_reservations).then((time:any) => {
      this.available_time = time;
      console.log("av_time",this.available_time)
    });
  }

  onChangedTime($event){
    console.log('date_storage',this.date_storange);
    this.reservationServ.getNumPeople($event,this.all_reservations,this.date_storange).then((num_people:any) => {
      this.available_people = num_people;
      console.log("av_num_people",this.available_people);
    });
  }
  
  submit(){    
    let selectedDate = new Date(this.reservation_date);
    let r_date = selectedDate.getFullYear()+'-';
    r_date += selectedDate.getMonth()+1+'-';
    r_date += selectedDate.getDate();
    this.reservation_date = r_date;
    let pre_reservation:any = {
      user_id: this.user_id,
      restaurant_id: this.restaurant_id || this.restaurant_info.$key,
      date: this.reservation_date,
      time: this.reservation_time,
      number_people: this.number_people,
      status: 10
    };
    console.log('info',pre_reservation);
    this.showPrompt(pre_reservation);
  }

  showPrompt(pre_reservation) {
    let person : any;
    if(pre_reservation.number_people == 1){
      person = "person";
    }else{
      person = "people";
    }
    let confirm = this.alertCtrl.create({
      title: 'Confirm Reservation',
      message: "You have reserved on "+pre_reservation.date+" at "+pre_reservation.time+" with "+pre_reservation.number_people+" "+person+" Note: Please be at least 15 mins before your reservation time",
      buttons: [        
        {
          text: 'Cancel',
          handler: () => {
            this.reservation_date = "";
            this.reservation_time = "";
            this.number_people = "";
          }
        },
        {
          text: 'Confirm',
          handler: () => {
              this.reservationServ.addReservation(pre_reservation).then((data) => {
                if(data){
                  LocalNotifications.schedule({
                     text: 'Your schedule is with in 1 hour please be reminded!',
                     at: new Date(new Date().getTime() + 3600),
                     led: 'FF0000',
                     sound: null
                  });
                  this.navCtrl.pop(ReservationPage);
                }
              });
          }
        }
      ]
    });
    confirm.present();
  }

}
