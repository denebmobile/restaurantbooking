import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {  AngularFireAuth, AngularFireDatabase, AngularFire } from 'angularfire2';
/*
  Generated class for the AuthData provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ReservationService {

restaurant_info : any;
available_date : any;
available_times: Array<any>;
to_available_times: Array<any>;
max_number_people : number = 10;

  constructor(
      public http: Http,
      public db: AngularFireDatabase, 
      public auth: AngularFireAuth,
      public af: AngularFire
      ) {
    
    console.log('Hello AppServ Provider');
    this.restaurant_info = JSON.parse(window.sessionStorage.getItem('restaurant_info'));

  }

  getDates(all_reservations){
    // original:
    let time_close = this.restaurant_info.time_close;

    // // for testing:
    // let time_close = 15;
    return new Promise((resolve, reject)=> {
      this.available_date = [];
      let days = 7;
      for(let i = 0; i < days; i++){
        let total = 0;
        let rightNow = new Date();
        if(i == 0 && rightNow.getHours()+1 > this.restaurant_info.time_close){
           rightNow.setDate(rightNow.getDate()+1);
        }
        rightNow.setDate(rightNow.getDate()+i);
        let r_date = rightNow.getFullYear()+'-';
        r_date += rightNow.getMonth()+1+'-';
        r_date += rightNow.getDate();

        for(let i = 0; i < all_reservations.length; i++){
          let reservation_time_storage = all_reservations[i].time.split(':');
          if(all_reservations[i].restaurant_id == this.restaurant_info.$key && all_reservations[i].date == r_date && rightNow.getHours()+1 >= parseInt(reservation_time_storage[0])){
            total += all_reservations[i].number_people;
          }
        }
        let multiplier = time_close - (rightNow.getHours()+1);
        if(total== (multiplier * this.max_number_people)){
          days++;
          continue;
        }
        
        this.available_date.push(rightNow);
      }
      resolve(this.available_date);
    });
  }

  getTime(date_selected, all_reservations){      
      console.log('restaurant_info',this.restaurant_info);
      return new Promise((resolve, reject)=> {
        let selectedDate = new Date(date_selected);
        let selectedDay = selectedDate.getDate();
        this.available_times = [];
        
        let t_now = new Date();
        let hour_now = t_now.getHours();
        let mins_now = t_now.getMinutes();
        let date_now = t_now.getDate();
        // original:
        let time_open = this.restaurant_info.time_open;
        let time_close = this.restaurant_info.time_close;

        // //for testing:
        // let time_open = 12;
        // let time_close = 15;

        let time_diff = time_close - time_open;
        let hours = time_close - time_open;

        for(let hour_counter = 0; hour_counter <= hours -1; hour_counter++ ){
            let bool = false;
            let hour = time_open + hour_counter;
                if(selectedDay == date_now){
                    if(hour_now >= hour){
                        continue;
                    }                   
                }
                for(let i = 0; i < all_reservations.length; i++){
                  let reservation_time_storage = all_reservations[i].time.split(':');
                  if(all_reservations[i].restaurant_id == this.restaurant_info.$key && all_reservations[i].number_people == this.max_number_people && hour==parseInt(reservation_time_storage[0])){
                    bool = true;
                  }
                }
                if(bool){
                  continue;
                }
                this.available_times.push({val: hour + ':00'});
        }
        resolve(this.available_times);
      });
      
  }
  
  getNumPeople(time, all_reservations, date_selected){
    return new Promise((resolve, reject)=> {
      let total = 0;
      let selectedDate = new Date(date_selected);
      let r_date = selectedDate.getFullYear()+'-';
      r_date += selectedDate.getMonth()+1+'-';
      r_date += selectedDate.getDate();
      for(let i = 0; i < all_reservations.length; i++){         
         let reservation_time_storage = all_reservations[i].time.split(':');
         let time_storange = time.split(':');
          if(all_reservations[i].restaurant_id == this.restaurant_info.$key && parseInt(time_storange[0])==parseInt(reservation_time_storage[0]) && all_reservations[i].date == r_date){
            total += all_reservations[i].number_people;
          }
      }
      let available_people = [];
      for(let i = 1; i <= this.max_number_people-total; i++){
        let text;
        let storage = {};
        if(i == 1){
          text = "person"
        }else{
          text = "people"
        }
        storage = {
          text: i+" "+text,
          val: i
        }
        available_people.push(storage);
      }
      resolve(available_people);
    });
    
  }

  addReservation(data){
    return this.db.list('reservations').push(data);
  }

}
